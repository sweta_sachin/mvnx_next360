package Ticketing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class ticketing_Tests extends CommonMethods{
	
	
	@BeforeClass()
	public void Setup() throws InterruptedException
	{
		Login();
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
	}
	
    @Test
    public void a_View_TicketingTab()
    {
    	view_ticketing();
    }
    
    @Test
    public void b_click_TicketingTab() throws InterruptedException
    {
    	Thread.sleep(1000);
    	click_ticketingTab();
    }
    
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*************************************** Method to view ticketing tab *************************************
public void view_ticketing()
 {
   	WebElement ticketing=  driver.findElement(By.linkText("Ticketing"));

   	Boolean Ticketing_tab= ticketing.isDisplayed();
    
   	Assert.assertTrue(Ticketing_tab);
    
 }
      
//**************************** Method to click Ticketing tab *****************************
public void click_ticketingTab()
{
   	 WebElement ticketing=  driver.findElement(By.linkText("Ticketing"));

   	 Actions act= new Actions(driver);
    
   	 act.moveToElement(ticketing).click().build().perform();
      
  }
}
